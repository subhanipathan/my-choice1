package com.gayathri.demo.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.gayathri.demo.dto.BikeDemo;

public class BikeDAo {
public static void main(String[] args) {
	BikeDemo bikeDemo= new BikeDemo();
	bikeDemo.setColor("Black");
	bikeDemo.setModel("ninja");
	bikeDemo.setPrice(400000.00);
	//hibernate code
	
	Configuration configuration= new Configuration();
	configuration.configure();
	configuration.addResource("bikeDemo.hbm.xml");
	SessionFactory factory=configuration.buildSessionFactory();
	Session session=factory.openSession();
	Transaction transaction=session.getTransaction();
	transaction.begin();
	session.save(bikeDemo);
	transaction.commit();
	session.close();
	
}
}
