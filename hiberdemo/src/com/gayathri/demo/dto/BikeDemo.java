package com.gayathri.demo.dto;


	import org.hibernate.dialect.MySQL5Dialect;

	
	public abstract class BikeDemo {		
			
			private int id;
			private String color;
			private String model;
			private double price;
			public int getId() {
				return id;
			}
			public void setId(int id) {
				this.id = id;
			}
			public String getColor() {
				return color;
			}
			public void setColor(String color) {
				this.color = color;
			}
			public String getModel() {
				return model;
			}
			public void setModel(String model) {
				this.model = model;
			}
			public double getPrice() {
				return price;
			}
			public void setPrice(double price) {
				this.price = price;
			}
			@Override
			public String toString() {
				return "BikeDto [id=" + id + ", color=" + color + ", model=" + model + ", price=" + price + "]";
			}
			
		}



